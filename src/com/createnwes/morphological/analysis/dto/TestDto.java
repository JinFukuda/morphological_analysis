package com.createnwes.morphological.analysis.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TestDto {
	
	private String parse1;
	
	private String parse2;

	/**
	 * parse1を取得します。
	 * @return parse1
	 */
	public String getParse1() {
	    return parse1;
	}

	/**
	 * parse1を設定します。
	 * @param parse1 parse1
	 */
	public void setParse1(String parse1) {
	    this.parse1 = parse1;
	}

	/**
	 * parse2を取得します。
	 * @return parse2
	 */
	public String getParse2() {
	    return parse2;
	}

	/**
	 * parse2を設定します。
	 * @param parse2 parse2
	 */
	public void setParse2(String parse2) {
	    this.parse2 = parse2;
	}
	
	

}
