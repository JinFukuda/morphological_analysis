package com.createnwes.morphological.analysis.dto;

public class Dto {
	
	private String surface;
	
	private String feature;

	/**
	 * surfaceを取得します。
	 * @return surface
	 */
	public String getSurface() {
	    return surface;
	}

	/**
	 * surfaceを設定します。
	 * @param surface surface
	 */
	public void setSurface(String surface) {
	    this.surface = surface;
	}

	/**
	 * featureを取得します。
	 * @return feature
	 */
	public String getFeature() {
	    return feature;
	}

	/**
	 * featureを設定します。
	 * @param feature feature
	 */
	public void setFeature(String feature) {
	    this.feature = feature;
	}
}
