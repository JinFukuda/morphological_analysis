package com.createnwes.morphological.analysis.helper;

import java.util.ArrayList;
import java.util.List;

import org.chasen.mecab.Tagger;

import com.createnwes.morphological.analysis.dto.Dto;
import com.createnwes.morphological.analysis.dto.ListDto;

public class MorphologicalAnalysis {
	
	
	static {
		try {
			System.loadLibrary("MeCab");
		} catch (UnsatisfiedLinkError e) {
			e.printStackTrace();
			System.err.println("Cannot load the example native code.\nMake sure your LD_LIBRARY_PATH contains \'.\'\n" + e);
			System.exit(1);
		}
	}

	public static ListDto execute(String s){
		
		ListDto listDto = new ListDto();
		
		if(s == null || "".equals(s)){
			
			listDto.setStatus(200);
			
		}
		
		Tagger tagger = new Tagger();
		String str = tagger.parse(s);
		
		String[] array = str.split("\n");
		
		List<Dto> list = new ArrayList<Dto>(); 
		
		for(String parse : array){
			
			String[] parse2 = parse.split("	");
			
			Dto dto = new Dto();
			
			for(int i = 0; i < parse2.length; i++){
				if(i == 0){
					dto.setSurface(parse2[i]);
				}else{
					dto.setFeature(parse2[i].split(",")[0]);
				}
			}
			
			if(!"EOS".equals(dto.getSurface())){
				list.add(dto);
			}
		}
		
		listDto.setDtoList(list);
		listDto.setStatus(0);
		
		return listDto;
		
	}

}
