package com.createnwes.morphological.analysis;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.createnwes.morphological.analysis.dto.TestDto;

@Path("test")
public class Test {

	@GET
	@Produces(MediaType.APPLICATION_XML)
	public TestDto test(){
		
		TestDto dto = new TestDto();
		
		dto.setParse1("test1");
		dto.setParse2("test2");
		
		return dto;
		
	}
	
}
