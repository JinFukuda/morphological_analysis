package com.createnwes.morphological.analysis;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.createnwes.morphological.analysis.dto.ListDto;
import com.createnwes.morphological.analysis.helper.MorphologicalAnalysis;

@Path("xml")
public class Xml {

	@GET
	@Produces(MediaType.APPLICATION_XML)
	public ListDto getXml(@QueryParam("s") String s){
		
		return MorphologicalAnalysis.execute(s);
		
	}
	
	@POST
	@Produces(MediaType.APPLICATION_XML)
	public ListDto postXml(@QueryParam("s") String s){
		
		return MorphologicalAnalysis.execute(s);
		
	}
	
}
