package com.createnwes.morphological.analysis.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ListDto {

	private int status;
	
	private List<Dto> word;

	public int getStatus(){
		return status;
	}
	
	public void setStatus(int status){
		this.status = status;
	}
	
	/**
	 * dtoListを取得します。
	 * @return dtoList
	 */
	public List<Dto> getDtoList() {
	    return word;
	}

	/**
	 * dtoListを設定します。
	 * @param dtoList dtoList
	 */
	public void setDtoList(List<Dto> dtoList) {
	    this.word = dtoList;
	}
	
}
