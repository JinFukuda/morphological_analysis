package com.createnwes.morphological.analysis;

import com.createnwes.morphological.analysis.dto.ListDto;
import com.createnwes.morphological.analysis.helper.MorphologicalAnalysis;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("json")
public class Json {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ListDto getXml(@QueryParam("s") String s){
		
		return MorphologicalAnalysis.execute(s);
		
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public ListDto postXml(@QueryParam("s") String s){
		
		return MorphologicalAnalysis.execute(s);
		
	}

}
